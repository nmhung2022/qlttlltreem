﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLSVThucTap.ViewModel
{
    public class TreEmVM
    {
        public string MaTreEm { get; set; }
        public string TenTreEm { get; set; }
        public string GioiTinh { get; set; }
        public DateTime NgaySinh { get; set; }
        public DateTime NgayNhapHoc { get; set; }
        public int CanNang { get; set; }
        public int ChieuCao { get; set; }
        public string TinhTrangSucKhoe { get; set; }
        public string DiaChi { get; set; }
        public string MaLop { get; set; }
        public string TenLop { get; set; }

    }
}
