﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLSVThucTap.ViewModel
{
    internal class LopVM
    {
        public string MaLop { get; set; }
        public string TenLop { get; set; }
    }
}
