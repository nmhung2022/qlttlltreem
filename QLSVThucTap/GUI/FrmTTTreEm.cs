﻿using ClosedXML.Excel;
using QLSVThucTap.BLL;
using QLSVThucTap.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using static QLSVThucTap.BLL.TreEmBLL;
using Excel = Microsoft.Office.Interop.Excel;


namespace QLSVThucTap
{
    public partial class FrmDSSVThucTap : Form
    {
        public FrmDSSVThucTap()
        {
            InitializeComponent();
        }

        public void NapTatCaTreEm()
        {
            var ListSV = TreEmBLL.getAllTreEm();
            treEmVMBindingSource.DataSource = ListSV;
            dataGridViewMain.DataSource = treEmVMBindingSource;
        }
        public void NapTreEmByLop(string malophocphan)
        {
            var ListSV = TreEmBLL.GetTreByIdLop(malophocphan);
            treEmVMBindingSource.DataSource = ListSV;
            dataGridViewMain.DataSource = treEmVMBindingSource;
        }

        public void loadTreeView()
        {
            treeView1.Nodes.Clear();
            TreeNode nodeAllKhoa = new TreeNode("Tất cả trẻ em");
            treeView1.Nodes.Add(nodeAllKhoa);
            var listLop = LopBLL.GetListLopHoc();
            foreach (LopVM item in listLop)
            {
                TreeNode nodeLop = new TreeNode(item.TenLop);
                nodeLop.Tag = item;
                nodeAllKhoa.Nodes.Add(nodeLop);
            }
            treeView1.ExpandAll();
        }


        private void HienThiLenComBoBox()
        {
            cmbLop.Items.Clear();
            foreach (LopVM d in LopBLL.GetListLopHoc())
            {
                cmbLop.Items.Add(d);
                cmbLop.DisplayMember = "TenLop";
                cmbLop.ValueMember = "MaLop";
            }
        }

        private void FrmThucTapSV_Load(object sender, EventArgs e)
        {
            loadTreeView();
            HienThiLenComBoBox();
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node != null)
            {
                if (e.Node.Level == 0)
                {
                    NapTatCaTreEm();
                }
                else if (e.Node.Level == 1)
                {
                    LopVM lop = e.Node.Tag as LopVM;
                    NapTreEmByLop(lop.MaLop);
                }
                else
                {
                    dataGridViewMain.DataSource = null;
                }
            }
        }

        private void HienThiLenComboBoxLop()
        {
            foreach (LopVM l in LopBLL.GetListLopHoc())
            {
                cmbLop.Items.Add(l);
                cmbLop.DisplayMember = "TenLop";
                cmbLop.ValueMember = "MaLop";
            }
        }



        private void btnThem_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            var MaTreEm = txtMTE.Text;
            var HoVaTen = txtHoVaTen.Text;
            DateTime NgayS = dateTimePickerNS.Value;
            DateTime NgayNhapHoc = dateTimePickerNNH.Value;
            var Lop = cmbLop.SelectedItem as LopVM;
            var GioiTinh = rdbNam.Checked ? true : false;
            var DiaChi = txtDiaChi.Text;
            var CanNang = txtCanNang.Text;
            var ChieuCao = txtChieuCao.Text;
            var TinhTrangSucKhoe = txtTinhTrang.Text;


            if (string.IsNullOrEmpty(MaTreEm))
            {
                errorProvider1.SetError(txtMTE, "Mã của trẻ không được để trống");
                return;
            }

            if (string.IsNullOrEmpty(HoVaTen))
            {
                errorProvider1.SetError(txtHoVaTen, "Họ tên của trẻ không được để trống");
                return;
            }

            if (string.IsNullOrEmpty(CanNang))
            {
                errorProvider1.SetError(txtCanNang, "Cân nặng của trẻ không được để trống");
                return;
            }

            if (string.IsNullOrEmpty(ChieuCao))
            {
                errorProvider1.SetError(txtChieuCao, "Chiều cao của trẻ không được để trống");
                return;
            }

            if (string.IsNullOrEmpty(TinhTrangSucKhoe))
            {
                errorProvider1.SetError(txtTinhTrang, "Tình trạng sức khoẻ của trẻ không được để trống");
                return;
            }

            if (cmbLop.SelectedIndex == -1)
            {
                MessageBox.Show("Bạn chưa chọn lớp học cho trẻ");
                return;
            }
            var rs = KETQUA.THANHCONG;
            if (txtMTE.Enabled == true)
            {
                rs = TreEmBLL.AddTreEm(new TreEmVM
                {
                    MaTreEm = MaTreEm,
                    TenTreEm = HoVaTen,
                    NgaySinh = NgayS.Date,
                    GioiTinh = GioiTinh ? "Nữ" : "Nam",
                    CanNang = Convert.ToInt32(txtCanNang.Text),
                    ChieuCao = Convert.ToInt32(txtChieuCao.Text),
                    TinhTrangSucKhoe = TinhTrangSucKhoe,
                    DiaChi = DiaChi,
                    NgayNhapHoc = NgayNhapHoc,
                    MaLop = Lop.MaLop,
                }, Lop.MaLop);
                if (rs == KETQUA.THANHCONG)
                {
                    MessageBox.Show("Thêm trẻ vào lớp thành công !!!", "Thông báo");
                    NapTreEmByLop(Lop.MaLop);
                }
                else if (rs == KETQUA.TRUNGMASV)
                {
                    MessageBox.Show("Mã trẻ này đã tồn tại !!!", "Thông báo");
                }
            }
            else
            {
                rs = TreEmBLL.UpdateTreEm(new TreEmVM
                {
                    MaTreEm = MaTreEm,
                    TenTreEm = HoVaTen,
                    NgaySinh = NgayS.Date,
                    GioiTinh = GioiTinh ? "Nữ" : "Nam",
                    CanNang = Convert.ToInt32(txtCanNang.Text),
                    ChieuCao = Convert.ToInt32(txtChieuCao.Text),
                    TinhTrangSucKhoe = TinhTrangSucKhoe,
                    DiaChi = DiaChi,
                    NgayNhapHoc = NgayNhapHoc,
                    MaLop = Lop.MaLop,
                }, Lop.MaLop);
                if (rs == KETQUA.THANHCONG)
                {
                    MessageBox.Show("Cập nhật thông tin trẻ thành công !!!", "Thông báo");
                    NapTreEmByLop(Lop.MaLop);
                }
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            var SelectedTreEm = treEmVMBindingSource.Current as TreEmVM;
            if (SelectedTreEm != null)
            {
                var rs = MessageBox.Show("Bạn có thật sự muốn xoá", "Chú ý", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (rs == DialogResult.OK)
                {
                    TreEmBLL.DeleteTreEm(SelectedTreEm.MaTreEm);
                    treEmVMBindingSource.RemoveCurrent();
                    MessageBox.Show("Đã xoá sinh viên thành công", "Thông báo");
                }
            }
        }

        private void dataGridViewMain_SelectionChanged(object sender, EventArgs e)
        {
            var SelectedTreEm = treEmVMBindingSource.Current as TreEmVM;
            if (SelectedTreEm != null && SelectedTreEm.MaTreEm != null && SelectedTreEm.TenTreEm != null)
            {
                txtMTE.Text = SelectedTreEm.MaTreEm;
                txtMTE.Enabled = false;
                txtHoVaTen.Text = SelectedTreEm.TenTreEm;
                dateTimePickerNS.Value = SelectedTreEm.NgaySinh;
                dateTimePickerNNH.Value = SelectedTreEm.NgayNhapHoc;

                cmbLop.Text = SelectedTreEm.TenLop;
                txtCanNang.Text = Convert.ToString(SelectedTreEm.CanNang);
                txtChieuCao.Text = Convert.ToString(SelectedTreEm.ChieuCao);
                txtDiaChi.Text = SelectedTreEm.DiaChi;
                txtTinhTrang.Text = SelectedTreEm.TinhTrangSucKhoe;
                if (SelectedTreEm.GioiTinh == "Nam")
                    rdbNam.Checked = true;
                else
                    rdbNu.Checked = true;
            }
            else
            {
                txtMTE.Clear();
                txtMTE.Enabled = true;
                txtHoVaTen.Clear();
                cmbLop.ResetText();
                txtDiaChi.Clear();
                txtChieuCao.Clear();
                txtCanNang.Clear();
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtMTE.Clear();
            txtMTE.Enabled = true;
            txtHoVaTen.Clear();
            cmbLop.ResetText();
            txtDiaChi.Clear();
            txtChieuCao.Clear();
            txtCanNang.Clear();
            txtTinhTrang.Clear();
        }

        private void txtTimKiem_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            var tenTimKiem = txtTimKiem.Text;
            var listTreEm = TreEmBLL.SearchByName(tenTimKiem);
            treEmVMBindingSource.DataSource = listTreEm;
            dataGridViewMain.DataSource = treEmVMBindingSource;
        }

        private void exportDanhSachTreEm(object sender, EventArgs e)
        {
            saveFileDialog1.DefaultExt = ".xlsx";
            saveFileDialog1.Filter = "Excel Files|*.xlsx;*.xlsm";

            if (saveFileDialog1.ShowDialog() == DialogResult.OK && dataGridViewMain.Rows.Count > 0)
            {
                try
                {

                    Excel.Application xcel = new Excel.Application();
                    Excel.Workbook excelWorkbook = xcel.Application.Workbooks.Add(Type.Missing);

                    for (int i = 1; i < dataGridViewMain.Columns.Count + 1; i++)
                    {
                        xcel.Cells[1, i] = dataGridViewMain.Columns[i - 1].HeaderText;
                    }

                    for (int i = 0; i < dataGridViewMain.Rows.Count; i++)
                    {
                        for (int j = 0; j < dataGridViewMain.Columns.Count; j++)
                        {
                            xcel.Cells[i + 2, j + 1] = dataGridViewMain.Rows[i].Cells[j].Value;
                        }
                    }
                    Excel.Worksheet worksheet = xcel.Worksheets["Sheet1"];
                    worksheet.Name = "Danh Sách Trẻ";
                    xcel.Columns.AutoFit();
                    xcel.Visible = true;
                    excelWorkbook.SaveAs(saveFileDialog1.FileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSaveAsAccessMode.xlExclusive);
                    MessageBox.Show("Xuất dữ liệu thành công", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    xcel.Quit();
                    Marshal.ReleaseComObject(xcel);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }
    }
}


