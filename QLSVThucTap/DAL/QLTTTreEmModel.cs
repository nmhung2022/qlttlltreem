using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace QLSVThucTap.DAL
{
    public partial class QLTTTreEmModel : DbContext
    {
        public QLTTTreEmModel()
            : base("name=QLTTTreEmModel")
        {
        }

        public virtual DbSet<Lop> Lops { get; set; }
        public virtual DbSet<TreEm> TreEms { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Lop>()
                .Property(e => e.MaLop)
                .IsUnicode(false);

            modelBuilder.Entity<Lop>()
                .HasMany(e => e.TreEms)
                .WithRequired(e => e.Lop)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TreEm>()
                .Property(e => e.MaTreEm)
                .IsUnicode(false);

            modelBuilder.Entity<TreEm>()
                .Property(e => e.MaLop)
                .IsUnicode(false);
        }
    }
}
