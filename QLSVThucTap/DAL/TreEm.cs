namespace QLSVThucTap.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TreEm")]
    public partial class TreEm
    {
        [Key]
        [StringLength(50)]
        public string MaTreEm { get; set; }

        [Required]
        [StringLength(150)]
        public string TenTreEm { get; set; }

        public bool GioiTinh { get; set; }

        [Column(TypeName = "date")]
        public DateTime NgaySinh { get; set; }

        [Column(TypeName = "date")]
        public DateTime NgayNhapHoc { get; set; }

        public int CanNang { get; set; }

        public int ChieuCao { get; set; }

        [Required]
        [StringLength(1000)]
        public string TinhTrangSucKhoe { get; set; }

        [Required]
        [StringLength(150)]
        public string DiaChi { get; set; }

        [Required]
        [StringLength(50)]
        public string MaLop { get; set; }

        public virtual Lop Lop { get; set; }
    }
}
