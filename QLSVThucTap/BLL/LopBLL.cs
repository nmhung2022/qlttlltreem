﻿using QLSVThucTap.DAL;
using QLSVThucTap.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLSVThucTap.BLL
{
    public class LopBLL
    {
        internal static List<LopVM> GetListLopHoc()
        {
            QLTTTreEmModel model = new QLTTTreEmModel();

            var lsSV = model.Lops.Select(l => new LopVM
            {
                MaLop = l.MaLop,
                TenLop = l.TenLop,
            }).ToList();
            return lsSV;
        }
    }
}
