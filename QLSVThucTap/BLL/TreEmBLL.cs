﻿using QLSVThucTap.DAL;
using QLSVThucTap.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;

namespace QLSVThucTap.BLL
{
    public class TreEmBLL
    {
        public enum KETQUA
        {
            THANHCONG, TRUNGMASV
        }

        internal static List<TreEmVM> getAllTreEm()
        {
            QLTTTreEmModel model = new QLTTTreEmModel();

            var ls = from hs in model.TreEms
                     join lp in model.Lops on hs.MaLop equals lp.MaLop
                     select new
                     {
                         hs.MaTreEm,
                         hs.TenTreEm,
                         hs.GioiTinh,
                         hs.NgaySinh,
                         hs.Lop,
                         hs.CanNang,
                         hs.ChieuCao,
                         hs.TinhTrangSucKhoe,
                         hs.DiaChi,
                         hs.NgayNhapHoc,
                         lp.TenLop
                     };
            return ls.Select(sv => new TreEmVM
            {

                MaTreEm = sv.MaTreEm,
                TenTreEm = sv.TenTreEm,
                NgaySinh = (DateTime)sv.NgaySinh,
                GioiTinh = (bool)sv.GioiTinh ? "Nam" : "Nữ",
                DiaChi = sv.DiaChi,
                CanNang = (int)sv.CanNang,
                ChieuCao = (int)sv.ChieuCao,
                TinhTrangSucKhoe = sv.TinhTrangSucKhoe,
                NgayNhapHoc = sv.NgayNhapHoc,
                TenLop = sv.TenLop,
            }).ToList();
        }

        internal static List<TreEmVM> GetTreByIdLop(string malop)
        {
            QLTTTreEmModel model = new QLTTTreEmModel();

            var ls = from hs in model.TreEms
                     join lp in model.Lops on hs.MaLop equals lp.MaLop
                     where hs.MaLop == malop
                     select new
                     {
                         hs.MaTreEm,
                         hs.TenTreEm,
                         hs.GioiTinh,
                         hs.NgaySinh,
                         hs.Lop,
                         hs.CanNang,
                         hs.ChieuCao,
                         hs.TinhTrangSucKhoe,
                         hs.DiaChi,
                         hs.NgayNhapHoc,
                         lp.TenLop
                     };
            return ls.Select(sv => new TreEmVM
            {
                MaTreEm = sv.MaTreEm,
                TenTreEm = sv.TenTreEm,
                NgaySinh = (DateTime)sv.NgaySinh,
                GioiTinh = (bool)sv.GioiTinh ? "Nam" : "Nữ",
                DiaChi = sv.DiaChi,
                CanNang = (int)sv.CanNang,
                ChieuCao = (int)sv.ChieuCao,
                TinhTrangSucKhoe = sv.TinhTrangSucKhoe,
                NgayNhapHoc = sv.NgayNhapHoc,
                TenLop = sv.TenLop,
            }).ToList();
        }

        internal static void DeleteTreEm(string maTreEm)
        {
            try
            {
                QLTTTreEmModel model = new QLTTTreEmModel();
                var sv = model.TreEms.Where(e => e.MaTreEm == maTreEm).FirstOrDefault();
                if (sv != null)
                {
                    model.TreEms.Remove(sv);
                }
                else
                    throw new Exception("Trẻ em không tồn tại");
                model.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }
        internal static KETQUA AddTreEm(TreEmVM sv, string maLop)
        {
            try
            {
                QLTTTreEmModel model = new QLTTTreEmModel();

                var svM = model.TreEms.Where(e => e.MaTreEm == sv.MaTreEm).FirstOrDefault();
                if (svM != null)
                {
                    return KETQUA.TRUNGMASV;
                }
                svM = new TreEm
                {
                    MaTreEm = sv.MaTreEm,
                    TenTreEm = sv.TenTreEm,
                    NgaySinh = sv.NgaySinh,
                    GioiTinh = sv.GioiTinh == "Nam" ? false : true,
                    DiaChi = sv.DiaChi,
                    CanNang = sv.CanNang,
                    ChieuCao = sv.ChieuCao,
                    TinhTrangSucKhoe = sv.TinhTrangSucKhoe,
                    MaLop = sv.MaLop,
                    NgayNhapHoc = sv.NgayNhapHoc,
                };
                model.TreEms.Add(svM);
                model.SaveChanges();
                return KETQUA.THANHCONG;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }

        internal static KETQUA UpdateTreEm(TreEmVM sv, string maLop)
        {
            try
            {
                QLTTTreEmModel model = new QLTTTreEmModel();
                var svM = model.TreEms.Where(e => e.MaTreEm == sv.MaTreEm).FirstOrDefault();
                svM.TenTreEm = sv.TenTreEm;
                svM.NgaySinh = sv.NgaySinh;
                svM.GioiTinh = sv.GioiTinh == "Nam" ? false : true;
                svM.DiaChi = sv.DiaChi;
                svM.CanNang = sv.CanNang;
                svM.ChieuCao = sv.ChieuCao;
                svM.TinhTrangSucKhoe = sv.TinhTrangSucKhoe;
                svM.MaLop = sv.MaLop;
                svM.NgayNhapHoc = sv.NgayNhapHoc;

                model.SaveChanges();
                return KETQUA.THANHCONG;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }

        internal static List<TreEmVM> SearchByName(string tenTimKiem)
        {
            try
            {
                QLTTTreEmModel model = new QLTTTreEmModel();

                var ls = from hs in model.TreEms
                         join lp in model.Lops on hs.MaLop equals lp.MaLop
                         where hs.TenTreEm.Contains(tenTimKiem)
                         select new
                         {
                             hs.MaTreEm,
                             hs.TenTreEm,
                             hs.GioiTinh,
                             hs.NgaySinh,
                             hs.Lop,
                             hs.CanNang,
                             hs.ChieuCao,
                             hs.TinhTrangSucKhoe,
                             hs.DiaChi,
                             hs.NgayNhapHoc,
                             lp.TenLop
                         };

                return ls.Select(sv => new TreEmVM
                {
                    MaTreEm = sv.MaTreEm,
                    TenTreEm = sv.TenTreEm,
                    NgaySinh = (DateTime)sv.NgaySinh,
                    GioiTinh = (bool)sv.GioiTinh ? "Nam" : "Nữ",
                    DiaChi = sv.DiaChi,
                    CanNang = (int)sv.CanNang,
                    ChieuCao = (int)sv.ChieuCao,
                    TinhTrangSucKhoe = sv.TinhTrangSucKhoe,
                    NgayNhapHoc = sv.NgayNhapHoc,
                    TenLop = sv.TenLop,
                }).ToList();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }
    }
}
