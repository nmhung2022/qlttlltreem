USE [QLTTLLTreEm]
GO
/****** Object:  Table [dbo].[Lop]    Script Date: 1/9/2022 7:58:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lop](
	[MaLop] [varchar](50) NOT NULL,
	[TenLop] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Lop] PRIMARY KEY CLUSTERED 
(
	[MaLop] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TreEm]    Script Date: 1/9/2022 7:58:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TreEm](
	[MaTreEm] [varchar](50) NOT NULL,
	[TenTreEm] [nvarchar](150) NOT NULL,
	[GioiTinh] [bit] NOT NULL,
	[NgaySinh] [date] NOT NULL,
	[NgayNhapHoc] [date] NOT NULL,
	[CanNang] [int] NOT NULL,
	[ChieuCao] [int] NOT NULL,
	[TinhTrangSucKhoe] [nvarchar](1000) NOT NULL,
	[DiaChi] [nvarchar](150) NOT NULL,
	[MaLop] [varchar](50) NOT NULL,
 CONSTRAINT [PK_HocSinh] PRIMARY KEY CLUSTERED 
(
	[MaTreEm] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Lop] ([MaLop], [TenLop]) VALUES (N'Lop1', N'Lớp Mần')
INSERT [dbo].[Lop] ([MaLop], [TenLop]) VALUES (N'Lop2', N'Lớp Non')
INSERT [dbo].[Lop] ([MaLop], [TenLop]) VALUES (N'Lop3', N'Lớp Lá')
INSERT [dbo].[TreEm] ([MaTreEm], [TenTreEm], [GioiTinh], [NgaySinh], [NgayNhapHoc], [CanNang], [ChieuCao], [TinhTrangSucKhoe], [DiaChi], [MaLop]) VALUES (N'TreEm1', N'Nguyễn Văn Bé new', 1, CAST(N'2005-12-30' AS Date), CAST(N'2020-12-30' AS Date), 24, 21, N'Tốt', N'23 Lê Lợi, Huế', N'Lop2')
INSERT [dbo].[TreEm] ([MaTreEm], [TenTreEm], [GioiTinh], [NgaySinh], [NgayNhapHoc], [CanNang], [ChieuCao], [TinhTrangSucKhoe], [DiaChi], [MaLop]) VALUES (N'TreEm2', N'Nguyễn Văn Lớn', 1, CAST(N'2004-12-30' AS Date), CAST(N'2020-12-30' AS Date), 32, 153, N'Tốt', N'23 Nguyễn Trãi', N'Lop2')
ALTER TABLE [dbo].[TreEm]  WITH CHECK ADD  CONSTRAINT [FK_HocSinh_Lop] FOREIGN KEY([MaLop])
REFERENCES [dbo].[Lop] ([MaLop])
GO
ALTER TABLE [dbo].[TreEm] CHECK CONSTRAINT [FK_HocSinh_Lop]
GO
